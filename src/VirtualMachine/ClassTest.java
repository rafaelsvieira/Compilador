/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VirtualMachine;

import VirtualMachine.Instruction;
import VirtualMachine.ParseFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author rvieira
 */
public class ClassTest {

    public static void main(String[] args) throws IOException {
        ParseFile assembly = new ParseFile("/home/rvieira/prog.txt");
        String[] txt = assembly.getTextOfFile();
        ArrayList<Instruction> listInstr = new ArrayList<Instruction>();
        Instruction inst;
        for (int i = 0; i < txt.length; i++) {
            inst = new Instruction(txt[i], i);
            if (inst.getInstr() != null) {
                listInstr.add(inst);
            }
        }
        System.err.println(listInstr.toString());
        //Instruction inst = new Instruction(a,1);

        //System.out.println(inst.toString());
    }
}
