/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VirtualMachine;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author rvieira
 */
public class ExecInst {

    private ArrayList<String> stack = new ArrayList<>();
    private ArrayList<Instruction> arrayInst;
    private Instruction nextInst = new Instruction();
    private int stackPosit;
    private int execPosit;
    private JTextArea inputTextArea;
    private JTextArea outputTextArea;

    public ExecInst(ArrayList<Instruction> inst, JTextArea inputTextArea, JTextArea outputTextArea) {
        this.arrayInst = inst;
        this.execPosit = 0;
        this.inputTextArea = inputTextArea;
        this.outputTextArea = outputTextArea;
    }

    public ArrayList<String> getStack() {
        return this.stack;
    }

    public int getPositStack() {
        return this.stackPosit;
    }

    public int getPositExec() {
        return this.execPosit;
    }

    public void execNextInst() {
        //Executa uma instruição por vez
    }

    public void execAllInst() {
        this.execPosit = 0;

        int arrayInstSize = this.arrayInst.size();
        while (this.execPosit < arrayInstSize) {
            this.nextInst = this.arrayInst.get(this.execPosit);

            switch (this.nextInst.getInstr()) {
                case "LDC":
                    LDC(this.nextInst);
                    break;

                case "LDV":
                    LDV(this.nextInst);
                    break;

                case "ADD":
                    ADD();
                    break;

                case "SUB":
                    SUB();
                    break;

                case "MULT":
                    MULT();
                    break;

                case "DIVI":
                    DIV();
                    break;

                case "INV":
                    INV();
                    break;

                case "AND":
                    AND();
                    break;

                case "OR":
                    OR();
                    break;

                case "NEG":
                    NEG();
                    break;

                case "CME":
                    CME();
                    break;

                case "CMA":
                    CMA();
                    break;

                case "CEQ":
                    CEQ();
                    break;

                case "CDIF":
                    CDIF();
                    break;

                case "CMEQ":
                    CMEQ();
                    break;

                case "CMAQ":
                    CMAQ();
                    break;

                case "START":
                    START();
                    break;

                case "HLT":
                    HLT();
                    break;

                case "STR":
                    STR(this.nextInst);
                    break;

                case "JMP":
                    JMP(this.nextInst);
                    break;

                case "JMPF":
                    JMPF(this.nextInst);
                    break;

                case "NULL":
                    NULL();
                    break;

                case "RD":
                    RD();
                    break;

                case "PRN":
                    PRN();
                    break;

                case "ALLOC":
                    ALLOC(this.nextInst);
                    break;

                case "DALLOC":
                    DALLOC(this.nextInst);
                    break;

                case "CALL":
                    CALL(this.nextInst);
                    break;

                case "RETURN":
                    RETURN();
                    break;
            }

            this.execPosit++;
        }

    }

    private void LDC(Instruction inst) {
        this.stackPosit++;
        addValue(this.stackPosit, inst.getVar1());
    }

    private void LDV(Instruction inst) {
        this.stackPosit++;
        addValue(this.stackPosit, inst.getVar1());
    }

    private void ADD() {
        String result;
        result = String.valueOf(Integer.parseInt(this.stack.get(this.stackPosit - 1))
                + Integer.parseInt(this.stack.get(this.stackPosit)));
        this.stackPosit--;
        addValue(this.stackPosit, result);
    }

    private void SUB() {
        String result;
        result = String.valueOf(Integer.parseInt(this.stack.get(stackPosit - 1))
                - Integer.parseInt(this.stack.get(this.stackPosit)));
        this.stackPosit--;
        addValue(this.stackPosit, result);
    }

    private void MULT() {
        String result;
        result = String.valueOf(Integer.parseInt(this.stack.get(this.stackPosit - 1))
                * Integer.parseInt(this.stack.get(this.stackPosit)));
        this.stackPosit--;
        addValue(this.stackPosit, result);
    }

    private void DIV() {
        String result;
        result = String.valueOf(Integer.parseInt(this.stack.get(this.stackPosit - 1))
                / Integer.parseInt(this.stack.get(this.stackPosit)));
        this.stackPosit--;
        addValue(this.stackPosit, result);
    }

    private void INV() {
        String result;
        result = String.valueOf(Integer.parseInt(this.stack.get(this.stackPosit)) * -1);
        addValue(this.stackPosit, result);
    }

    private void AND() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1)) == 1
                && Integer.parseInt(this.stack.get(this.stackPosit)) == 1) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "0");
        }
        this.stackPosit--;
    }

    private void OR() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1)) == 1
                || Integer.parseInt(this.stack.get(this.stackPosit)) == 1) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "0");
        }
        this.stackPosit--;
    }

    private void NEG() {
        String result;
        result = String.valueOf(1 - Integer.parseInt(this.stack.get(this.stackPosit)));
        addValue(this.stackPosit, result);
    }

    private void CME() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                < Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void CMA() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                > Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void CEQ() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                == Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void CDIF() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                != Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void CMEQ() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                <= Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void CMAQ() {
        if (Integer.parseInt(this.stack.get(this.stackPosit - 1))
                >= Integer.parseInt(this.stack.get(this.stackPosit))) {
            addValue(this.stackPosit - 1, "1");
        } else {
            addValue(this.stackPosit - 1, "-1");
        }
        this.stackPosit--;
    }

    private void START() {
        this.stack.clear();
        this.stackPosit = -1;
    }

    private void HLT() {
        this.execPosit = this.arrayInst.size();
    }

    private void STR(Instruction inst) {
        addValue(Integer.parseInt(inst.getVar1()), this.stack.get(stackPosit));
        stackPosit--;
    }

    private void JMP(Instruction inst) {
        this.execPosit = (this.getPositLabel(inst.getVar1()) - 1);
    }

    private void JMPF(Instruction inst) {
        if (this.stack.get(stackPosit).equals("0")) {
            this.execPosit = (this.getPositLabel(inst.getVar1()) - 1);
        }
        this.stackPosit--;
    }

    private void NULL() {

    }

    private void RD() {
        String inputNum = null;
        while (validInput(inputNum) == false) {
            inputNum = JOptionPane.showInputDialog("Digite valor de entrada");
            if (validInput(inputNum) == false) {
                JOptionPane.showMessageDialog(null, "Você não digitou valor de entrada invalido");
            }
        }
        this.stackPosit++;
        addValue(stackPosit, inputNum);
        appendInputTextArea(inputNum);
    }

    private void PRN() {
        appendOutputTextArea(stack.get(stackPosit));
        stackPosit--;
    }

    private void ALLOC(Instruction inst) {
        int start_alloc = Integer.parseInt(inst.getVar1());
        int amnt_alloc = Integer.parseInt(inst.getVar2());

        for (int i = 0; i < amnt_alloc; i++) {
            this.stackPosit++;

            try {
                addValue(this.stackPosit, this.stack.get(start_alloc + i));
            } catch (Exception e) {
                addValue(this.stackPosit, null);
                addValue(this.stackPosit, this.stack.get(start_alloc + i));
            }
        }
    }

    private void DALLOC(Instruction inst) {
        int start_dalloc = Integer.parseInt(inst.getVar1());
        int amnt_dalloc = Integer.parseInt(inst.getVar2());
        for (int i = amnt_dalloc - 1; i >= 0; i--) {
            addValue(start_dalloc + i, this.stack.get(this.stackPosit));
            this.stackPosit--;
        }
    }

    private void CALL(Instruction inst) {
        this.stackPosit++;
        String value = String.valueOf(this.execPosit + 1);

        addValue(this.stackPosit, value);
        this.execPosit = (this.getPositLabel(inst.getVar1()) - 1);
    }

    private void RETURN() {
        this.execPosit = Integer.parseInt(this.stack.get(this.stackPosit)) - 1;
        stackPosit--;
    }

    private int getPositLabel(String label) {
        for (int i = 0; i < arrayInst.size(); i++) {
            if (label.equals(arrayInst.get(i).getVar1())
                    && arrayInst.get(i).getInstr().equals("NULL")) {
                return i;
            }
        }
        System.err.println("Error label[" + label + "] not found!");
        System.exit(0);
        return 0;
    }

    private boolean validInput(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private void addValue(int index, String value) {
        try {
            this.stack.set(index, value);
        } catch (Exception e) {
            this.stack.add(index, value);
        }
    }

    public void appendOutputTextArea(String text) {
        this.outputTextArea.append(text + "\n");
    }

    public void appendInputTextArea(String text) {
        this.inputTextArea.append(text + "\n");
    }
}
