/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VirtualMachine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rvieira
 */
public class ParseFile {

    private String pathFile;
    private ArrayList<String> listInstruc;

    public ParseFile() {

    }

    public ParseFile(String path) {
        this.pathFile = path;
    }

    public void setPathFile(String path) {
        this.pathFile = path;
    }

    public int getNumberOfLines() {
        /*Retorna numero total de linhas do arquivo. Auxilia na leitura.*/
        int numberOfLines = 0;
        FileReader fr;
        try {
            fr = new FileReader(pathFile);
            BufferedReader bf = new BufferedReader(fr);
            while (bf.readLine() != null) {
                numberOfLines++;
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Erro getNumberLines, parseFile.java #FileReader");
            System.exit(0);
        } catch (IOException ex) {
            System.err.println("Erro getNumberLines, parseFile.java #readLine");
            System.exit(0);
        }
        return numberOfLines;
    }

    public String[] getTextOfFile() {
        /*Retorn array de strings, cada posicao tem uma linha do arquivo.*/
        int lines = 0;
        FileReader fr;
        try {
            fr = new FileReader(pathFile);
            String[] textData;
            String lineRead;
            lines = getNumberOfLines();
            textData = new String[lines];
            int i = 0;
            BufferedReader bf = new BufferedReader(fr);
            while ((lineRead = bf.readLine()) != null) {
                textData[i] = lineRead;
                i++;
            }
            bf.close();
            fr.close();
            return textData;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ParseFile.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Erro readFile, parseFile.java");
            System.exit(0);
        }
        return null;
    }
}
