/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VirtualMachine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rvieira
 */
public class Instruction {

    private String instr;
    private String var1;
    private String va2;
    private int row;
    private Map ope = new HashMap();

    public Instruction() {

    }

    public Instruction(String inst, int row) {
        ope.put("LDC", 1);
        ope.put("LDV", 1);
        ope.put("ADD", 0);
        ope.put("SUB", 0);
        ope.put("MULT", 0);
        ope.put("DIVI", 0);
        ope.put("INV", 0);
        ope.put("AND", 0);
        ope.put("OR", 0);
        ope.put("NEG", 0);
        ope.put("CME", 0);
        ope.put("CMA", 0);
        ope.put("CEQ", 0);
        ope.put("CDIF", 0);
        ope.put("CMEQ", 0);
        ope.put("CMAQ", 0);
        ope.put("START", 0);
        ope.put("HLT", 0);
        ope.put("STR", 1);
        ope.put("JMP", 1);
        ope.put("JMPF", 1);
        ope.put("NULL", 1);
        ope.put("RD", 0);
        ope.put("PRN", 0);
        ope.put("ALLOC", 2);
        ope.put("DALLOC", 2);
        ope.put("CALL", 1);
        ope.put("RETURN", 0);
        this.validInstr(inst, row);
        ope.clear();
    }

    private void validInstr(String inst, int row) {
        int qtdVar;
        String msg = inst;
        inst = inst.replaceAll("\t", " ");
        inst = inst.replaceAll(",", " ");
        ArrayList<String> array_inst = new ArrayList<>(Arrays.asList(inst.split(" ")));
        array_inst.removeAll(Collections.singleton(""));

        if (array_inst.isEmpty()) {
            return;
        }

        if (ope.containsKey(array_inst.get(0))) {
            qtdVar = (int) ope.get(array_inst.get(0));

            if (qtdVar == 0) {
                this.setInstr(array_inst.get(0));
                this.setLinha(row);
                this.setVar1(null);
                this.setVar2(null);
                return;
            }
            if (qtdVar == 1) {
                this.setInstr(array_inst.get(0));
                this.setLinha(row);
                if (isDigit((array_inst.get(1)))
                        || array_inst.get(0).equals("JMP")
                        || array_inst.get(0).equals("JMPF")
                        | array_inst.get(0).equals("CALL")) {
                    this.setVar1(array_inst.get(1));
                    this.setVar2(null);
                } else {
                    System.err.println("Error in row [" + (row + 1) + "] --------> " + msg + " [Parameter is not a number]");
                    System.exit(0);
                }
                return;
            }
            if (qtdVar == 2) {
                this.setInstr(array_inst.get(0));
                this.setLinha(row);
                if (isDigit((array_inst.get(1))) && isDigit((array_inst.get(2)))) {
                    this.setVar1(array_inst.get(1));
                    this.setVar2(array_inst.get(2));
                } else {
                    System.err.println("Error in row [" + (row + 1) + "] --------> " + msg + " [Parameter is not a number]");
                    System.exit(0);
                }
                return;
            }
            System.err.println("Error in row [" + (row + 1) + "] --------> " + msg + " [wrong parameters]");
            System.exit(0);
        } else if (array_inst.get(1).equals("NULL")) {
            qtdVar = (int) ope.get(array_inst.get(1));
            if (qtdVar == 1) {
                this.setInstr(array_inst.get(1));
                this.setLinha(row);
                this.setVar1(array_inst.get(0));
                this.setVar2(null);
            } else {
                System.err.println("Error in row [" + (row + 1) + "] --------> " + msg + " [wrong parameters]");
                System.exit(0);
            }

        } else {
            System.err.println("Error in row [" + (row + 1) + "] --------> " + msg + " [Instruction does not exist]");
            System.exit(0);
        }
    }

    private boolean isDigit(String str) {
        try {
            int num = Integer.parseInt(str);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void setInstr(String instr) {
        this.instr = instr;
    }

    public void setVar1(String var1) {
        this.var1 = var1;
    }

    public void setVar2(String va2) {
        this.va2 = va2;
    }

    public void setLinha(int linha) {
        this.row = linha;
    }

    public String getInstr() {
        return instr;
    }

    public String getVar1() {
        return var1;
    }

    public String getVar2() {
        return va2;
    }

    public int getLinha() {
        return row;
    }

    @Override
    public String toString() {
        return "Instruction{" + "instr=" + instr + ", var1=" + var1 + ", va2=" + va2 + ", row=" + row + '}';
    }

}
