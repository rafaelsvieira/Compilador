/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lexico;

import java.util.ArrayList;
import VirtualMachine.ParseFile;
import javax.swing.JTextArea;

/**
 *
 * @author rvieira
 */
public class AnalisadorLexico {

    private JTextArea saidaCompilador;
    private int numeroToken;
    private ArrayList<Token> tokens = new ArrayList<>();
    private String codigo;
    private char caracter;
    private String linhaAtual;
    private int posicaoCaracter;
    private int numeroLinha;
    boolean acabou = false;

    public AnalisadorLexico() {

    }

    public AnalisadorLexico(String codigo, JTextArea saidaCompilador) {
        this.saidaCompilador = saidaCompilador;
        this.numeroLinha = 0;
        this.posicaoCaracter = -1;
        this.linhaAtual = "";
        this.codigo = codigo;
        this.numeroToken = -1;

        leCaracter();
        /*
        while (!this.acabou) {
            consomeComentarioEspaco();
            pegaToken();
        }
         */
    }

    public ArrayList<Token> getTokens() {
        return this.tokens;
    }

    public Token getToken() {
        consomeComentarioEspaco();
        if (this.acabou) {
            return null;
        }
        pegaToken();
        this.numeroToken++;
        return tokens.get(this.numeroToken);
    }

    public void pegaToken() {
        //  leCaracter();
        if (Character.isDigit(this.caracter)) {
            trataDigito();
        } else if (Character.isLetter(this.caracter)) {
            trataIdentificadorPalavraReservada();
        } else if (":".equals(String.valueOf(this.caracter))) {
            trataAtribuicao();
        } else if ("+".equals(String.valueOf(this.caracter)) || ("-".equals(String.valueOf(this.caracter))) || ("*".equals(String.valueOf(this.caracter)))) {
            trataOperadorAritmetico();
        } else if ("<".equals(String.valueOf(this.caracter)) || (">".equals(String.valueOf(this.caracter))) || ("=".equals(String.valueOf(this.caracter))) || ("!".equals(String.valueOf(this.caracter)))) {
            trataOperadorRelacional();
        } else if (".".equals(String.valueOf(this.caracter)) || (";".equals(String.valueOf(this.caracter))) || (",".equals(String.valueOf(this.caracter))) || ("(".equals(String.valueOf(this.caracter))) || (")".equals(String.valueOf(this.caracter)))) {
            trataPontuacao();
        } else {
            //erro;
            this.acabou = true; 
            this.saidaCompilador.append("Linha ("
                    + (this.numeroLinha + 2)
                    + "): Erro: caractere "
                    + "\""
                    + this.caracter
                    + "\""
                    + " não é aceito pela linguagem. "
                    + "\n");
        }
    }

    private void leCaracter() {
        this.posicaoCaracter++;
        if (this.posicaoCaracter >= this.codigo.length()) {
            this.acabou = true;
        } else {
            this.caracter = this.codigo.charAt(this.posicaoCaracter);
        }

    }

    private void consomeComentarioEspaco() {
        while (this.caracter == ' '
                || this.caracter == '\t'
                || this.caracter == '{'
                || this.caracter == '\n'
                && !this.acabou) {

            if (this.caracter == '{') {
                leCaracter();
                while (true && !this.acabou) {
                    leCaracter();
                    if (this.caracter == '}') {
                        leCaracter();
                        break;
                    }
                }
            } else if (this.caracter == '\n') {
                this.numeroLinha++;
                leCaracter();
            } else {
                leCaracter();
            }
        }
    }

    private void trataDigito() {
        String num = "";
        num += this.caracter;
        leCaracter();
        while (Character.isDigit(this.caracter)) {
            num += caracter;
            leCaracter();
        }
        tokens.add(new Token(num, "snumero", this.numeroLinha));
    }

    private void trataIdentificadorPalavraReservada() {
        String id = "";
        id += this.caracter;
        leCaracter();
        while (Character.isLetterOrDigit(this.caracter) || this.caracter == '_') {
            id += this.caracter;
            leCaracter();
        }
        switch (id) {
            case "programa":
                tokens.add(new Token(id, "sprograma", this.numeroLinha));
                break;
            case "se":
                tokens.add(new Token(id, "sse", this.numeroLinha));
                break;
            case "entao":
                tokens.add(new Token(id, "sentao", this.numeroLinha));
                break;
            case "senao":
                tokens.add(new Token(id, "ssenao", this.numeroLinha));
                break;
            case "enquanto":
                tokens.add(new Token(id, "senquanto", this.numeroLinha));
                break;
            case "faca":
                tokens.add(new Token(id, "sfaca", this.numeroLinha));
                break;
            case "inicio":
                tokens.add(new Token(id, "sinicio", this.numeroLinha));
                break;
            case "fim":
                tokens.add(new Token(id, "sfim", this.numeroLinha));
                break;
            case "escreva":
                tokens.add(new Token(id, "sescreva", this.numeroLinha));
                break;
            case "leia":
                tokens.add(new Token(id, "sleia", this.numeroLinha));
                break;
            case "var":
                tokens.add(new Token(id, "svar", this.numeroLinha));
                break;
            case "inteiro":
                tokens.add(new Token(id, "sinteiro", this.numeroLinha));
                break;
            case "booleano":
                tokens.add(new Token(id, "sbooleano", this.numeroLinha));
                break;
            case "procedimento":
                tokens.add(new Token(id, "sprocedimento", this.numeroLinha));
                break;
            case "funcao":
                tokens.add(new Token(id, "sfuncao", this.numeroLinha));
                break;
            case "div":
                tokens.add(new Token(id, "sdiv", this.numeroLinha));
                break;
            case "e":
                tokens.add(new Token(id, "se", this.numeroLinha));
                break;
            case "ou":
                tokens.add(new Token(id, "sou", this.numeroLinha));
                break;
            case "nao":
                tokens.add(new Token(id, "snao", this.numeroLinha));
                break;
            default:
                tokens.add(new Token(id, "sidentificador", this.numeroLinha));
                break;
        }
    }

    private void trataAtribuicao() {
        String atrib = "";
        atrib += this.caracter;
        leCaracter();
        if ("=".equals(String.valueOf(this.caracter))) {
            atrib += this.caracter;
            tokens.add(new Token(atrib, "satribuicao", this.numeroLinha));
            leCaracter();
        } else {
            tokens.add(new Token(atrib, "sdoispontos", this.numeroLinha));
        }
    }

    private void trataOperadorAritmetico() {
        String op = "";
        op += this.caracter;
        if ("+".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(op, "smais", this.numeroLinha));
        } else if ("-".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(op, "smenos", this.numeroLinha));
        } else if ("*".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(op, "smult", this.numeroLinha));
        }
        leCaracter();
    }

    private void trataOperadorRelacional() {
        String op = "";
        op += this.caracter;
        if ("<".equals(String.valueOf(this.caracter))) {
            leCaracter();
            if ("=".equals(String.valueOf(this.caracter))) {
                op += this.caracter;
                tokens.add(new Token(op, "smenorigual", this.numeroLinha));
                leCaracter();
            } else {
                tokens.add(new Token(op, "smenor", this.numeroLinha));
            }
        } else if (">".equals(String.valueOf(this.caracter))) {
            leCaracter();
            if ("=".equals(String.valueOf(this.caracter))) {
                op += this.caracter;
                tokens.add(new Token(op, "smaiorigual", this.numeroLinha));
                leCaracter();
            } else {
                tokens.add(new Token(op, "smaior", this.numeroLinha));
            }
        } else if ("=".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(op, "sigual", this.numeroLinha));
            leCaracter();
        } else if ("!".equals(String.valueOf(this.caracter))) {
            leCaracter();
            if ("=".equals(String.valueOf(this.caracter))) {
                op += this.caracter;
                tokens.add(new Token(op, "sdiferente", this.numeroLinha));
                leCaracter();
            } else {
                //erro;
                this.acabou = true;
                this.saidaCompilador.append("Linha ("
                        + (this.numeroLinha + 1)
                        + "): Erro: caractere \"!\" não está acompanhado do caractere \"=\". "
                        + "\n");
            }
        }
    }

    private void trataPontuacao() {
        String pont = "";
        pont += this.caracter;
        if (".".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(pont, "sponto", this.numeroLinha));
        } else if (";".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(pont, "spontovirgula", this.numeroLinha));
        } else if (",".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(pont, "svirgula", this.numeroLinha));
        } else if ("(".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(pont, "sabreparenteses", this.numeroLinha));
        } else if (")".equals(String.valueOf(this.caracter))) {
            tokens.add(new Token(pont, "sfechaparenteses", this.numeroLinha));
        }
        leCaracter();
    }
}
