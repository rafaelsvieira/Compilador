/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lexico;

/**
 *
 * @author lhrenzo
 */
public class Token {

    private String lexema;
    private String simbolo;
    private int numeroSimbolo;
    private int linha;

    public Token() {

    }

    public Token(Token token) {
        this.lexema = token.getLexema();
        this.simbolo = token.getSimbolo();
        this.numeroSimbolo = token.getNumerSimbolo();
        this.linha = token.getLinha();
    }

    public Token(String lexema, String simbolo, int linha) {
        this.lexema = lexema;
        this.linha = linha;
        SimbolosEnum simb = SimbolosEnum.valueOf(simbolo);
        this.simbolo = simb.getSimbolo();
        this.numeroSimbolo = simb.getNum();
    }

    public void setToken(Token token) {
        this.lexema = token.getLexema();
        this.simbolo = token.getSimbolo();
        this.numeroSimbolo = token.getNumerSimbolo();
        this.linha = token.getLinha();
    }

    public void setToken(String lexema, String simbolo, int linha) {
        this.lexema = lexema;
        SimbolosEnum simb = SimbolosEnum.valueOf(simbolo);
        this.simbolo = simb.getSimbolo();
        this.numeroSimbolo = simb.getNum();
        this.linha = linha;
    }

    public String getLexema() {
        return this.lexema;
    }

    public String getSimbolo() {
        return this.simbolo;
    }

    public int getNumerSimbolo() {
        return this.numeroSimbolo;
    }

    public int getLinha() {
        return this.linha;
    }
}
