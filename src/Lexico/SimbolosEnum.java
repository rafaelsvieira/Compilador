/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lexico;

/**
 *
 * @author rvieira
 */
public enum SimbolosEnum {
    sprograma("sprograma", 1),
    sinicio("sinicio", 2),
    sfim("sfim", 3),
    sprocedimento("sprocedimento", 4),
    sfuncao("sfuncao", 5),
    sse("sse", 6),
    sentao("sentao", 7),
    ssenao("ssenao", 8),
    senquanto("senquanto", 9),
    sfaca("sfaca", 10),
    satribuicao("satribuicao", 11),
    sescreva("sescreva", 12),
    sleia("sleia", 13),
    svar("svar", 14),
    sinteiro("sinteiro", 15),
    sbooleano("sbooleano", 16),
    sidentificador("sidentificador", 17),
    snumero("snumero", 18),
    sponto("sponto", 19),
    spontovirgula("spontovirgula", 20),
    svirgula("svirgula", 21),
    sabreparenteses("sabreparenteses", 22),
    sfechaparenteses("sfechaparenteses", 23),
    smaior("smaior", 24),
    smaiorigual("smaiorigual", 25),
    sig("sig", 26),
    smenor("smenor", 27),
    smenorigual("smenorigual", 28),
    sdiferente("sdiferente", 29),
    smais("smais", 30),
    smenos("smenos", 31),
    smult("smult", 32),
    sdiv("sdiv", 33),
    se("se", 34),
    sou("sou", 35),
    snao("snao", 36),
    sdoispontos("sdoispontos", 37),
    sverdadeiro("sverdadeiro", 38),
    sfalso("sfalso", 39);

    private int valorSimbolos;
    private String nomeSimbolos;

    SimbolosEnum(String simbolo, int valor) {
        this.nomeSimbolos = simbolo;
        this.valorSimbolos = valor;
    }

    public String getSimbolo() {
        return this.nomeSimbolos;
    }

    public int getNum() {
        return this.valorSimbolos;
    }
}
